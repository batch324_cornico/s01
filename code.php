<?php 
$address1 = (object)[
	'houseNumber' => '3F Caswyn Bldg.',
	'street' => 'Timog Avenue',
	'city' => 'Quezon City',
	'state' => 'Metro Manila',
	'country' => 'Philippines'
];

$address2 = (object)[
	'houseNumber' => '3F Enzo Bldg.',
	'street' => 'Buendia Avenue',
	'city' => 'Makati City',
	'state' => 'Metro Manila',
	'country' => 'Philippines'
];

function getLetterGrade($grades){
	if ($grades <= 74){
		return 'D';
	}
	else if ($grades <= 76 && $grades >= 75){
		return 'C-';
	}
	else if($grades <= 79 && $grades >= 77){
		return 'C';
	}
	else if ($grades <=82 && $grades >=80){
		return 'C+';
	}
	else if ($grades <=85 && $grades >=83){
		return 'B-';
	}
	else if($grades <=88 && $grades >=86){
		return 'B';
	}
	else if($grades <=91 && $grades >=89){
		return 'B+';
	}
	else if ($grades <=94 && $grades >=92){
		return 'A-';
	}
	else if($grades <=97 && $grades >=95){
		return 'A';
	}
	else{
		return 'A+';
	}
	

}






 ?>